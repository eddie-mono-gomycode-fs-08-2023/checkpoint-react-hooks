import React from 'react';
import MovieCard from './MovieCard';
// - This component will display a list of movies based on the provided data and the current filters
const MovieList = ({ movies, filterTitle, filterRating }) => {
  const filteredMovies = movies.filter(
    (movie) =>
      movie.title.toLowerCase().includes(filterTitle.toLowerCase()) &&
      movie.rating >= filterRating
  );

  return (
    <div className="movie-list"  style={{display: 'flex', flexWrap: 'Wrap', backgroundColor: 'grey'}}>
      {filteredMovies.map((movie, index) => (
        <MovieCard key={index} {...movie} />
      ))}
    </div>
  );
};

export default MovieList;