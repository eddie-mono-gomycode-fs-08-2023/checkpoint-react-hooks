import React from 'react';
// This component will display individual movie information.
const MovieCard = ({ title, description, posterURL, rating }) => {
  return (
    <div className="movie-card" style={{marginRight:'20px'}}>
      <img src={posterURL} alt={title} />
      <h2>{title}</h2>
      <p>{description}</p>
      <p>Rating: {rating}</p>
    </div>
  );
};

export default MovieCard;