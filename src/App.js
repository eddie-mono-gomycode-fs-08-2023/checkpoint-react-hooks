import React, { useState } from 'react';
import MovieCard from './MovieCard';
import MovieList from './MovieList';
import Filter from './Filter';
// This is my movie data and components integration
function App() {
  const [movies, setMovies] = useState([
  // Initial movie data
  { 
    title: 'The Expendables',
    description: 'The will die when they are dead',
    posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/mOX5O6JjCUWtlYp5D8wajuQRVgy.jpg',
      rating: 4
    },
{ 
  title: 'The Ritual Killer',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/1VtHaELqLMovl8EueBWHDtihhzF.jpg',
    rating: 3 
  },
  {
    title: 'The Equalizer',
  description: 'Justice knows no borders.',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/b0Ej6fnXAP8fK75hlyi2jKqdhHz.jpg',
    rating: 3 
  },
  { 
    title: 'Balerinna',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/oE7xtGDqZnr7tFHfwb8oM9iRW6H.jpg',
    rating: 3 
  },
  {
    title: 'Nowhere',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/k2kbdwwmVKJp6VMUcJYKlL9jQ7d.jpg',
    rating: 3 
  },
  { 
    title: 'Blue Beatle',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/mXLOHHc1Zeuwsl4xYKjKh2280oL.jpg',
    rating: 3 
  },
  {
    title: '57Seconds',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/dfS5qHWFuXyZQnwYREwb7N4qU5p.jpg',
    rating: 3 
  },
  { 
    title: 'Fast',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/fiVW06jE7z9YnO4trhaMEdclSiC.jpg',
    rating: 3 
  },
  { 
    title: 'Fast',
  description: 'Unable to process the death of his daughter,',
  posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/fiVW06jE7z9YnO4trhaMEdclSiC.jpg',
    rating: 3 
},
    { 
      title: 'Fast',
    description: 'Unable to process the death of his daughter,',
    posterURL: 'https://www.themoviedb.org/t/p/w220_and_h330_face/fiVW06jE7z9YnO4trhaMEdclSiC.jpg',
      rating: 3 
    },
  
// Add more initial movies here
]);

  const [filterTitle, setFilterTitle] = useState('');
  const [filterRating, setFilterRating] = useState(0);

  const addMovie = (newMovie) => {
    setMovies([...movies, newMovie]);
  };

  return (
    <div className="App">
      <h1>Movie App</h1>
      <Filter
        filterTitle={filterTitle}
        setFilterTitle={setFilterTitle}
        filterRating={filterRating}
        setFilterRating={setFilterRating}
      />
      <MovieList movies={movies} filterTitle={filterTitle} filterRating={filterRating} />
    </div>
  );
}

export default App;