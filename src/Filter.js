import React from 'react';
// Setting title and rating filters.
const Filter = ({ filterTitle, setFilterTitle, filterRating, setFilterRating }) => {
  return (
    <div className="filter">
      <input
        type="text"
        placeholder="Filter by Title"
        value={filterTitle}
        onChange={(e) => setFilterTitle(e.target.value)}
      />
      <input
        type="number"
        placeholder="Filter by Rating"
        value={filterRating}
        onChange={(e) => setFilterRating(e.target.value)}
      />
    </div>
  );
};

export default Filter;